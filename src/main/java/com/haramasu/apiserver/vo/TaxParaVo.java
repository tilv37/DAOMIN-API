package com.haramasu.apiserver.vo;

import com.haramasu.apiserver.constant.Wuxianjishu;

import java.math.BigDecimal;

/**
 * @Auther: DingShuo
 * @Date: 2018/11/19 17:34
 * @Description:
 */
public class TaxParaVo {
    double preTaxSalary;
    double taxbaseSalary;
    double gjjBase;
    double gjjRate;

    public double getPreTaxSalary() {
        return preTaxSalary;
    }

    public void setPreTaxSalary(double preTaxSalary) {
        this.preTaxSalary = preTaxSalary;
    }

    public double getTaxbaseSalary() {
        BigDecimal low=new BigDecimal(Wuxianjishu.SHE_BAO_BASE_LOW);
        BigDecimal high=new BigDecimal(Wuxianjishu.SHE_BAO_BASE_HIGH);
        BigDecimal base=new BigDecimal(this.taxbaseSalary);

        if(base.compareTo(low)<0){
            return low.doubleValue();
        }

        if(base.compareTo(high)>0){
            return high.doubleValue();
        }
        return taxbaseSalary;
    }

    public void setTaxbaseSalary(double taxbaseSalary) {
        this.taxbaseSalary = taxbaseSalary;
    }

    public double getGjjBase() {
        return gjjBase;
    }

    public void setGjjBase(double gjjBase) {
        this.gjjBase = gjjBase;
    }

    public double getGjjRate() {

        BigDecimal low=new BigDecimal(Wuxianjishu.GJJ_RATE_LOW);
        BigDecimal high=new BigDecimal(Wuxianjishu.GJJ_RATE_HIGH);
        BigDecimal base=new BigDecimal(this.gjjRate);

        if(base.compareTo(low)<0){
            return low.doubleValue();
        }

        if(base.compareTo(high)>0){
            return high.doubleValue();
        }

        return gjjRate;
    }

    public void setGjjRate(double gjjRate) {
        this.gjjRate = gjjRate;
    }
}
