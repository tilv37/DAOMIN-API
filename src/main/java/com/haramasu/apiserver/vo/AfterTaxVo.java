package com.haramasu.apiserver.vo;

/**
 * @Auther: DingShuo
 * @Date: 2018/11/19 17:21
 * @Description:
 */
public class AfterTaxVo {
    double preTaxSalary;
    double afterTaxSalary;
    double yangLaoFee;
    double yangLaoRate;
    double yiLiaoFee;
    double yiLiaoRate;
    double daEYiLiaoFee;
    double shiYeFee;
    double shiYeRate;
    double gjjFee;

    public double getPreTaxSalary() {
        return preTaxSalary;
    }

    public void setPreTaxSalary(double preTaxSalary) {
        this.preTaxSalary = preTaxSalary;
    }

    public double getAfterTaxSalary() {
        return afterTaxSalary;
    }

    public void setAfterTaxSalary(double afterTaxSalary) {
        this.afterTaxSalary = afterTaxSalary;
    }

    public double getYangLaoFee() {
        return yangLaoFee;
    }

    public void setYangLaoFee(double yangLaoFee) {
        this.yangLaoFee = yangLaoFee;
    }

    public double getYangLaoRate() {
        return yangLaoRate;
    }

    public void setYangLaoRate(double yangLaoRate) {
        this.yangLaoRate = yangLaoRate;
    }

    public double getYiLiaoFee() {
        return yiLiaoFee;
    }

    public void setYiLiaoFee(double yiLiaoFee) {
        this.yiLiaoFee = yiLiaoFee;
    }

    public double getYiLiaoRate() {
        return yiLiaoRate;
    }

    public void setYiLiaoRate(double yiLiaoRate) {
        this.yiLiaoRate = yiLiaoRate;
    }

    public double getDaEYiLiaoFee() {
        return daEYiLiaoFee;
    }

    public void setDaEYiLiaoFee(double daEYiLiaoFee) {
        this.daEYiLiaoFee = daEYiLiaoFee;
    }

    public double getShiYeFee() {
        return shiYeFee;
    }

    public void setShiYeFee(double shiYeFee) {
        this.shiYeFee = shiYeFee;
    }

    public double getShiYeRate() {
        return shiYeRate;
    }

    public void setShiYeRate(double shiYeRate) {
        this.shiYeRate = shiYeRate;
    }

    public double getGjjFee() {
        return gjjFee;
    }

    public void setGjjFee(double gjjFee) {
        this.gjjFee = gjjFee;
    }
}
