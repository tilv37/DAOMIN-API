package com.haramasu.apiserver.api;

import com.haramasu.apiserver.service.TaxService;
import com.haramasu.apiserver.vo.AfterTaxVo;
import com.haramasu.apiserver.vo.TaxParaVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Auther: DingShuo
 * @Date: 2018/11/19 17:20
 * @Description:
 */
@RestController
@RequestMapping(value = "/api/v1/")
public class TaxApi {
    @Autowired
    TaxService taxService;


    @PostMapping(value = "wh")
    public AfterTaxVo calWuHanTaxSalary(@RequestBody TaxParaVo taxParaVo){
        return taxService.calTax(taxParaVo);
    }
}
