package com.haramasu.apiserver.constant;

/**
 * @Auther: DingShuo
 * @Date: 2018/11/19 17:25
 * @Description:
 */
public interface Wuxianjishu {

    final static double SHE_BAO_BASE_LOW=3399.6;
    final static double SHE_BAO_BASE_HIGH=19921.0;
    final static double YANG_LAO_RATE=0.08;
    final static double YI_LIAO_RATE=0.02;
    final static double DA_E_YI_LIAO_BASE=7.0;
    final static double SHI_YE_RATE=0.003;
    final static double GJJ_RATE_LOW=0.05;
    final static double GJJ_RATE_HIGH=0.12;

}
