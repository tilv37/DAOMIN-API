package com.haramasu.apiserver.service;

import com.haramasu.apiserver.vo.AfterTaxVo;
import com.haramasu.apiserver.vo.TaxParaVo;

/**
 * @Auther: DingShuo
 * @Date: 2018/11/19 17:36
 * @Description:
 */
public interface TaxService {

    AfterTaxVo calTax(TaxParaVo taxParaVo);
}
