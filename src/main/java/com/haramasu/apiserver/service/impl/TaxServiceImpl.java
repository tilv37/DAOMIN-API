package com.haramasu.apiserver.service.impl;

import com.haramasu.apiserver.constant.Wuxianjishu;
import com.haramasu.apiserver.service.TaxService;
import com.haramasu.apiserver.vo.AfterTaxVo;
import com.haramasu.apiserver.vo.TaxParaVo;
import org.springframework.stereotype.Service;

/**
 * @Auther: DingShuo
 * @Date: 2018/11/19 17:37
 * @Description:
 */
@Service
public class TaxServiceImpl implements TaxService {

    @Override
    public AfterTaxVo calTax(TaxParaVo taxParaVo) {

        AfterTaxVo afterTaxVo=salaryWithOutWuxian(taxParaVo);
        double after=salaryWithOutTax(afterTaxVo.getAfterTaxSalary());
        afterTaxVo.setAfterTaxSalary(after);
        return afterTaxVo;
    }

    private AfterTaxVo salaryWithOutWuxian(TaxParaVo taxParaVo){
        AfterTaxVo afterTaxVo=new AfterTaxVo();
        afterTaxVo.setShiYeRate(Wuxianjishu.SHI_YE_RATE);
        afterTaxVo.setYangLaoRate(Wuxianjishu.YANG_LAO_RATE);
        afterTaxVo.setYiLiaoRate(Wuxianjishu.YI_LIAO_RATE);
        afterTaxVo.setDaEYiLiaoFee(7);
        afterTaxVo.setPreTaxSalary(taxParaVo.getPreTaxSalary());
        double yanglao=taxParaVo.getTaxbaseSalary()*Wuxianjishu.YANG_LAO_RATE;
        double yiliao=taxParaVo.getTaxbaseSalary()*Wuxianjishu.YI_LIAO_RATE;
        double daeyiliao=7;
        double shiye=taxParaVo.getTaxbaseSalary()*Wuxianjishu.SHI_YE_RATE;

        afterTaxVo.setYangLaoFee(yanglao);
        afterTaxVo.setYiLiaoFee(yiliao);
        afterTaxVo.setDaEYiLiaoFee(daeyiliao);
        afterTaxVo.setShiYeFee(shiye);

        double gjj=taxParaVo.getGjjBase()*taxParaVo.getGjjRate();

        afterTaxVo.setGjjFee(gjj);

        double afterWuxian=taxParaVo.getTaxbaseSalary()-yanglao-yiliao-daeyiliao-shiye-gjj;

        afterTaxVo.setAfterTaxSalary(afterWuxian);


        return afterTaxVo;
    }

    public  double salaryWithOutTax(double salary){

        double[] taxRate=new double[]{0.03,0.1,0.20,0.25,0.30,0.35,0.45};

        double[] salarybase=new double[]{3000,12000,25000,35000,55000,80000,Double.MAX_VALUE};

        double[] salaryminus=new double[]{0,210,1410,2660,4410,7160,15160};

        double afterSalary=0;

        if(salary<5000){
            return 0;
        }

        double salaryLevel=salary-5000;

        for (int i=0;i<7;i++){
            if(salaryLevel<=salarybase[i]){
                double tax=salaryLevel*taxRate[i]-salaryminus[i];
                afterSalary=  salary-tax;
                break;
            }
        }
        return afterSalary;
    }

//    public static void main(String[] args){
//        System.out.println(salaryWithOutTax(18368));
//    }

}
